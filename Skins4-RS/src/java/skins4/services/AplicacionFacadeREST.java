/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package skins4.services;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import skins4.ejb.AplicacionFacade;
import skins4.ejb.GrupoFacade;
import skins4.ejb.UsuarioFacade;
import skins4.entity.Aplicacion;

/**
 *
 * @author diegoojedagarcia
 */
@Stateless
@Path("skins4.restful.aplicacion")
public class AplicacionFacadeREST  {
    @EJB
    private UsuarioFacade usuarioFacade;
    @EJB
    private GrupoFacade grupoFacade;
    @EJB
    private AplicacionFacade aplicacionFacade;
    
    public AplicacionFacadeREST() {
    }

    @GET
    @Path("fetchAppsGivenGroup")
    @Produces ("application/json")
    public List<Aplicacion> fetchAppsGivenGroup (@QueryParam("idGrupo") int idGrupo){
        List<Aplicacion> lista = aplicacionFacade.findAplicacionesGrupo(grupoFacade.find(BigDecimal.valueOf(idGrupo)));
        return lista;
    }

    @GET
    @Path("fetchApps")
    @Produces("application/json")
    public List<Aplicacion> fetchAplicaciones(@QueryParam("email") String email){
        List<Aplicacion> lista = aplicacionFacade.findAppByUsuario(usuarioFacade.findByEmail(email));
        return lista;
    }
}
