/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.services;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import skins4.ejb.AplicacionFacade;
import skins4.ejb.ComponentexaplicacionFacade;
import skins4.entity.Componentexaplicacion;

@Stateless
@Path("skins4.restful.componentexaplicacion")
public class ComponenteXAplicacionFacadeREST {
    @EJB
    ComponentexaplicacionFacade componentexaplicacionFacade;

    @EJB
    AplicacionFacade aplicacionFacade;
    
    public ComponenteXAplicacionFacadeREST () {
        
    }

    @GET
    @Path ("getComponentesAplicacion")
    @Produces ("application/json")
    public List<Componentexaplicacion> getComponentesAplicacion (@QueryParam("idAplicacion") int id){
        return componentexaplicacionFacade.obtenerComponenteDeAplicacion(aplicacionFacade.find(BigDecimal.valueOf(id)));
    }

}
