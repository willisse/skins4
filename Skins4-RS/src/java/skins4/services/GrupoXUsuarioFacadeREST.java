package skins4.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import skins4.ejb.GrupoFacade;
import skins4.ejb.GrupoxusuarioFacade;
import skins4.ejb.UsuarioFacade;
import skins4.entity.Grupo;
import skins4.entity.Grupoxusuario;
import skins4.entity.Usuario;

@Stateless
@Path("skins4.restful.grupoxusuario")
public class GrupoXUsuarioFacadeREST {
    @EJB
    private GrupoFacade grupoFacade;
    @EJB
    private GrupoxusuarioFacade grupoxusuarioFacade;
    @EJB
    private UsuarioFacade usuarioFacade;

    public GrupoXUsuarioFacadeREST() {
        
    }

    @GET
    @Path("fetchUsuariosGroup")
    @Produces ("application/json")
    public List<Usuario> fetchUsuariosGroup (@QueryParam("idGrupo") int idGrupo){
        List<Grupoxusuario> l = grupoxusuarioFacade.getAllUsuariosGrupo(grupoFacade.find(BigDecimal.valueOf(idGrupo)));
        List<Usuario> usuarios = new ArrayList<>();
        for (Grupoxusuario u : l){
            usuarios.add(u.getIdusuario());
        }
        return usuarios;
    }
    
    @GET
    @Path("fetchGruposUsuario")
    @Produces ("application/json")
    public List<Grupo> fetchGruposUsuario (@QueryParam ("idUsuario") int idUsuario){
        return grupoxusuarioFacade.getAllGrupos(usuarioFacade.find(BigDecimal.valueOf(idUsuario)));
        
    }
}
