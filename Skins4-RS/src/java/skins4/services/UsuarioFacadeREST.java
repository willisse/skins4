/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package skins4.services;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import skins4.ejb.UsuarioFacade;
import skins4.entity.Usuario;

/**
 *
 * @author diegoojedagarcia
 */
@Stateless
@Path("skins4.restful.usuario")
public class UsuarioFacadeREST {
    @EJB
    private UsuarioFacade usuarioFacade;
    
    
    public UsuarioFacadeREST() {
    }
    
    @POST
    @Path("doLogin")
    @Consumes("application/json")
    @Produces("application/json")
    public Usuario doLogin(@HeaderParam("email") String email, @HeaderParam ("password") String password){
        Usuario user = usuarioFacade.findByEmail(email);
        if (user.getPassword().equals(password)){
            return user;
        }
        return null;
    }

}
