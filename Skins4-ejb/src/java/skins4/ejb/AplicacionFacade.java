/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package skins4.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import skins4.entity.Aplicacion;
import skins4.entity.Grupo;
import skins4.entity.Usuario;

@Stateless
public class AplicacionFacade extends AbstractFacade<Aplicacion> {
    @PersistenceContext(unitName = "Skins4-ejbPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public AplicacionFacade() {
        super(Aplicacion.class);
    }
    
    public void flush(){
        em.flush();
    }
    
    public synchronized Aplicacion createAndReturn(Aplicacion a) {
        this.create(a);
        return (Aplicacion) em.createQuery("SELECT a FROM Aplicacion a ORDER BY a.idAplicacion DESC").getResultList().get(0);
    }
    
    
    
    public List<Aplicacion> findAplicacionesGrupo(Grupo grupo) {
        return em.createNamedQuery("Aplicacion.findByGrupo").setParameter("idGrupo", grupo).getResultList();
        
    }
    
    
    
    public List<Aplicacion> findAppByUsuario(Usuario usuario) {
        
        return em.createNamedQuery("Aplicacion.findByIdUsuario").setParameter("idUsuario", usuario).getResultList();
        
    }
}
