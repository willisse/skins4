/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import skins4.entity.Componente;

/**
 *
 * @author masterinftel21
 */
@Stateless
public class ComponenteFacade extends AbstractFacade<Componente> {
    @PersistenceContext(unitName = "Skins4-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ComponenteFacade() {
        super(Componente.class);
    }
    
    public Componente findByName(String name){
        
        Query query1 = em.createQuery("SELECT c FROM Componente c WHERE c.nombre LIKE :value").setParameter("value", name);
        
        Componente comp;
        
        try{
            comp = (Componente) query1.getSingleResult();
        }
        catch(NoResultException e){
            comp = null;
        }
        
        return comp;
    }
}
