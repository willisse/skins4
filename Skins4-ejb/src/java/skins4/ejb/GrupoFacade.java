/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skins4.ejb;

import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import skins4.entity.Aplicacion;
import skins4.entity.Grupo;
import skins4.entity.Grupoxusuario;

/**
 *
 * @author masterinftel21
 */
@Stateless
public class GrupoFacade extends AbstractFacade<Grupo> {

   
    @PersistenceContext(unitName = "Skins4-ejbPU")
    private EntityManager em;
 
    @EJB
    private AplicacionFacade aplicacionFacade;
    
    @EJB
    private GrupoxusuarioFacade grupoxusuarioFacade;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GrupoFacade() {
        super(Grupo.class);
    }

    public synchronized Grupo createAndReturn(Grupo g) {
        this.create(g);
        return (Grupo) em.createQuery("SELECT g FROM Grupo g ORDER BY g.idGrupo DESC").getResultList().get(0);
    }

    @Override
    public void create(Grupo entity) {
        super.create(entity); //To change body of generated methods, choose Tools | Templates.
        Grupo g = (Grupo) em.createQuery("SELECT g FROM Grupo g ORDER BY g.idGrupo DESC").getResultList().get(0);
        Grupoxusuario gu = new Grupoxusuario(BigDecimal.ZERO);
        gu.setIdgrupo(g);
        gu.setIdusuario(g.getPropietario());
        grupoxusuarioFacade.create(gu);
        getEntityManager().flush();

    }

    @Override
    public void remove(Grupo grupo) {
        for (Aplicacion app : aplicacionFacade.findAplicacionesGrupo(grupo)) {
            app.setGrupo(null);
            aplicacionFacade.edit(app);
        }
        super.remove(grupo); //To change body of generated methods, choose Tools | Templates.
//         for (Grupoxusuario gu : grupo.getGrupoxusuarioList()) {
//            grupoxusuarioFacade.remove(gu);
//        }
    }
}
