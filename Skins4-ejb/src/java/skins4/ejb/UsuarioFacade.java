/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import skins4.entity.Grupo;
import skins4.entity.Usuario;

/**
 *
 * @author masterinftel21
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {
    @PersistenceContext(unitName = "Skins4-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    public Usuario findByEmail(String email){
        
        Query query1 = em.createQuery("SELECT u FROM Usuario u WHERE u.login LIKE :value").setParameter("value", email);
        
        Usuario user;
        
        try{
            user = (Usuario) query1.getSingleResult();
        }
        catch(NoResultException e){
            user = null;
        }
        
        return user;
    }
    
    public synchronized Usuario createAndReturn(Usuario u) {
        this.create(u);
        return (Usuario) em.createQuery("SELECT u FROM Usuario u ORDER BY u.idUsuario DESC").getResultList().get(0);
    }
}
