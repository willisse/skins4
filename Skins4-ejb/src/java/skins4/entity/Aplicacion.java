/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author masterinftel22
 */
@Entity
@Table(name = "APLICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aplicacion.findByGrupo", query = "SELECT a FROM Aplicacion a WHERE a.grupo = :idGrupo ORDER BY a.idAplicacion DESC"),
    @NamedQuery(name = "Aplicacion.findByIdUsuario", query = "SELECT a FROM Aplicacion a WHERE a.propietario = :idUsuario ORDER BY a.idAplicacion DESC"),
    @NamedQuery(name = "Aplicacion.findAll", query = "SELECT a FROM Aplicacion a"),
    @NamedQuery(name = "Aplicacion.findByIdAplicacion", query = "SELECT a FROM Aplicacion a WHERE a.idAplicacion = :idAplicacion"),
    @NamedQuery(name = "Aplicacion.findByNombre", query = "SELECT a FROM Aplicacion a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Aplicacion.findByDescripcion", query = "SELECT a FROM Aplicacion a WHERE a.descripcion = :descripcion"),
    @NamedQuery(name = "Aplicacion.findByFecha", query = "SELECT a FROM Aplicacion a WHERE a.fecha = :fecha")})
public class Aplicacion implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_APLICACION")
    private BigDecimal idAplicacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "PROPIETARIO", referencedColumnName = "ID_USUARIO")
    @ManyToOne(optional = false)
    private Usuario propietario;
    @JoinColumn(name = "GRUPO", referencedColumnName = "ID_GRUPO")
    @ManyToOne
    private Grupo grupo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idaplicacion")
    private List<Componentexaplicacion> componentexaplicacionList;

    public Aplicacion() {
    }

    public Aplicacion(BigDecimal idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public Aplicacion(BigDecimal idAplicacion, String nombre, String descripcion, Date fecha) {
        this.idAplicacion = idAplicacion;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fecha = fecha;
    }

    public BigDecimal getIdAplicacion() {
        return idAplicacion;
    }

    public void setIdAplicacion(BigDecimal idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        System.out.println("Fecha: " + fecha);
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Usuario getPropietario() {
        return propietario;
    }

    public void setPropietario(Usuario propietario) {
        this.propietario = propietario;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    @XmlTransient
    public List<Componentexaplicacion> getComponentexaplicacionList() {
        return componentexaplicacionList;
    }

    public void setComponentexaplicacionList(List<Componentexaplicacion> componentexaplicacionList) {
        this.componentexaplicacionList = componentexaplicacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAplicacion != null ? idAplicacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aplicacion)) {
            return false;
        }
        Aplicacion other = (Aplicacion) object;
        if ((this.idAplicacion == null && other.idAplicacion != null) || (this.idAplicacion != null && !this.idAplicacion.equals(other.idAplicacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "skins4.entity.Aplicacion[ idAplicacion=" + idAplicacion + " ]";
    }
    
}
