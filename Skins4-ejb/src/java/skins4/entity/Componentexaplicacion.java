/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author masterinftel22
 */
@Entity
@Table(name = "COMPONENTEXAPLICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Componentexaplicacion.findAll", query = "SELECT c FROM Componentexaplicacion c"),
    @NamedQuery(name = "Componentexaplicacion.findByIdComponentexaplicacion", query = "SELECT c FROM Componentexaplicacion c WHERE c.idComponentexaplicacion = :idComponentexaplicacion")})
public class Componentexaplicacion implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "COLUMNA")
    private String columna;
    @Basic(optional = false)
    @Size(min = 1, max = 225)
    @Column(name = "DETALLES")
    private String detalles;
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_COMPONENTEXAPLICACION")
    private BigDecimal idComponentexaplicacion;
    @JoinColumn(name = "IDCOMPONENTE", referencedColumnName = "ID_COMPONENTE")
    @ManyToOne(optional = false)
    private Componente idcomponente;
    @JoinColumn(name = "IDAPLICACION", referencedColumnName = "ID_APLICACION")
    @ManyToOne(optional = false)
    private Aplicacion idaplicacion;

    public Componentexaplicacion() {
    }

    public Componentexaplicacion(BigDecimal idComponentexaplicacion) {
        this.idComponentexaplicacion = idComponentexaplicacion;
    }

    public BigDecimal getIdComponentexaplicacion() {
        return idComponentexaplicacion;
    }

    public void setIdComponentexaplicacion(BigDecimal idComponentexaplicacion) {
        this.idComponentexaplicacion = idComponentexaplicacion;
    }

    public Componente getIdcomponente() {
        return idcomponente;
    }

    public void setIdcomponente(Componente idcomponente) {
        this.idcomponente = idcomponente;
    }

    public Aplicacion getIdaplicacion() {
        return idaplicacion;
    }

    public void setIdaplicacion(Aplicacion idaplicacion) {
        this.idaplicacion = idaplicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComponentexaplicacion != null ? idComponentexaplicacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Componentexaplicacion)) {
            return false;
        }
        Componentexaplicacion other = (Componentexaplicacion) object;
        if ((this.idComponentexaplicacion == null && other.idComponentexaplicacion != null) || (this.idComponentexaplicacion != null && !this.idComponentexaplicacion.equals(other.idComponentexaplicacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "skins4.entity.Componentexaplicacion[ idComponentexaplicacion=" + idComponentexaplicacion + " ]";
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getColumna() {
        return columna;
    }

    public void setColumna(String columna) {
        this.columna = columna;
    }
    
}
