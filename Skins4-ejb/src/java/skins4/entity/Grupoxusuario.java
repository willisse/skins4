/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author masterinftel22
 */
@Entity
@Table(name = "GRUPOXUSUARIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupoxusuario.findByIdUsuario", query = "SELECT g FROM Grupoxusuario g WHERE g.idusuario.idUsuario = :idusuario ORDER BY g.idGrupoxusuario DESC"),
    @NamedQuery(name = "Grupoxusuario.findByIdGrupo", query = "SELECT g FROM Grupoxusuario g WHERE g.idgrupo.idGrupo = :idgrupo"),
    @NamedQuery(name = "Grupoxusuario.findAll", query = "SELECT g FROM Grupoxusuario g"),
    @NamedQuery(name = "Grupoxusuario.findByIdGrupoxusuario", query = "SELECT g FROM Grupoxusuario g WHERE g.idGrupoxusuario = :idGrupoxusuario")})
public class Grupoxusuario implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_GRUPOXUSUARIO")
    private BigDecimal idGrupoxusuario;
    @JoinColumn(name = "IDUSUARIO", referencedColumnName = "ID_USUARIO")
    @ManyToOne(optional = false)
    private Usuario idusuario;
    @JoinColumn(name = "IDGRUPO", referencedColumnName = "ID_GRUPO")
    @ManyToOne(optional = false)
    private Grupo idgrupo;

    public Grupoxusuario() {
    }

    public Grupoxusuario(BigDecimal idGrupoxusuario) {
        this.idGrupoxusuario = idGrupoxusuario;
    }

    public BigDecimal getIdGrupoxusuario() {
        return idGrupoxusuario;
    }

    public void setIdGrupoxusuario(BigDecimal idGrupoxusuario) {
        this.idGrupoxusuario = idGrupoxusuario;
    }

    public Usuario getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Usuario idusuario) {
        this.idusuario = idusuario;
    }

    public Grupo getIdgrupo() {
        return idgrupo;
    }

    public void setIdgrupo(Grupo idgrupo) {
        this.idgrupo = idgrupo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrupoxusuario != null ? idGrupoxusuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupoxusuario)) {
            return false;
        }
        Grupoxusuario other = (Grupoxusuario) object;
        if ((this.idGrupoxusuario == null && other.idGrupoxusuario != null) || (this.idGrupoxusuario != null && !this.idGrupoxusuario.equals(other.idGrupoxusuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "skins4.entity.Grupoxusuario[ idGrupoxusuario=" + idGrupoxusuario + " ]";
    }
    
}
