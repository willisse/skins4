/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skins4.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import skins4.ejb.AplicacionFacade;
import skins4.ejb.ComponenteFacade;
import skins4.ejb.ComponentexaplicacionFacade;
import skins4.entity.Aplicacion;
import skins4.entity.Componente;
import skins4.entity.Componentexaplicacion;
import skins4.entity.Usuario;

/**
 *
 * @author masterinftel21
 */
@ManagedBean
@SessionScoped
public class EditorBean implements Serializable {

    @EJB
    private AplicacionFacade aplicacionFacade;

    @EJB
    private ComponentexaplicacionFacade componentexaplicacionFacade;

    @EJB
    private ComponenteFacade componenteFacade;

    private List<Componente> itemsDisponibles;
    private List<Componente> itemsDerecha;
    private List<Componente> itemsIzquierda;
    private String cadena;

    private Aplicacion app;
    private Usuario user;
    private LoginBean loginBean;

    public EditorBean() {
    }

    public Aplicacion getApp() {
        return app;
    }

    public void setApp(Aplicacion app) {
        this.app = app;
    }

    public List<Componente> getItemsDisponibles() {
        return itemsDisponibles;
    }

    public void setItemsDisponibles(List<Componente> itemsDisponibles) {
        this.itemsDisponibles = itemsDisponibles;
    }

    public List<Componente> getItemsDerecha() {
        return itemsDerecha;
    }

    public void setItemsDerecha(List<Componente> itemsDerecha) {
        this.itemsDerecha = itemsDerecha;
    }

    public List<Componente> getItemsIzquierda() {
        return itemsIzquierda;
    }

    public void setItemsIzquierda(List<Componente> itemsIzquierda) {
        this.itemsIzquierda = itemsIzquierda;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;

        add2list(cadena);
    }

    public void add2list(String cadena) {

        Componente nuevo = new Componente();

        String destino = cadena.substring(0, 3);
        String origen = cadena.substring(3, 6);
        String componente = cadena.substring(6, cadena.length());

        if (destino.equalsIgnoreCase("Izq")) {
            nuevo = componenteFacade.findByName(componente);
            if (nuevo != null) {
                if (itemsIzquierda == null) {
                    itemsIzquierda = new ArrayList<>();
                }
                this.itemsIzquierda.add(nuevo);
            }
        } else if (destino.equalsIgnoreCase("Der")) {
            nuevo = componenteFacade.findByName(componente);
            if (nuevo != null) {
                if (itemsDerecha == null) {
                    itemsDerecha = new ArrayList<>();
                }
                this.itemsDerecha.add(nuevo);
            }
        }

        if (!origen.equalsIgnoreCase("Fun")) {

            if (destino.equalsIgnoreCase("Fun")) {
                nuevo = componenteFacade.findByName(componente);
            }

            if (origen.equalsIgnoreCase("Izq")) {
                this.itemsIzquierda.remove(nuevo);
            } else {
                this.itemsDerecha.remove(nuevo);
            }
        }

    }

    public String cargarItems() {

        this.itemsDerecha = componentexaplicacionFacade.obtenerComponentes("Der", app);
        this.itemsIzquierda = componentexaplicacionFacade.obtenerComponentes("Izq", app);

        if (itemsDerecha == null) {
            itemsDerecha = new ArrayList<>();
        }

        if (itemsIzquierda == null) {
            itemsIzquierda = new ArrayList<>();
        }

        return "editor";
    }

    public String guardarAplicacion() {

        componentexaplicacionFacade.eliminarComp(app);

        if (itemsDerecha != null) {
            for (int i = 0; i < this.itemsDerecha.size(); i++) {
                Componentexaplicacion compXApp = new Componentexaplicacion();
                compXApp.setIdComponentexaplicacion(BigDecimal.ZERO);
                compXApp.setIdcomponente(this.itemsDerecha.get(i));
                compXApp.setIdaplicacion(app);
                compXApp.setColumna("Der");
                compXApp.setDetalles(null);
                componentexaplicacionFacade.create(compXApp);
            }
        }

        if (itemsIzquierda != null) {
            for (int i = 0; i < this.itemsIzquierda.size(); i++) {
                Componentexaplicacion compXApp = new Componentexaplicacion();
                compXApp.setIdComponentexaplicacion(BigDecimal.ZERO);
                compXApp.setIdcomponente(this.itemsIzquierda.get(i));
                compXApp.setIdaplicacion(app);
                compXApp.setColumna("Izq");
                compXApp.setDetalles(null);
                componentexaplicacionFacade.create(compXApp);
            }
        }

        clean();
        return "aplicaciones";
    }

    public String salir() {
        clean();
        return "index";
    }

    public void clean() {
        itemsDerecha = null;
        itemsIzquierda = null;
    }

    @PostConstruct
    void cargar() {
        itemsDisponibles = new ArrayList<>();
        itemsDisponibles = componenteFacade.findAll();
        itemsDerecha = new ArrayList<>();
        itemsIzquierda = new ArrayList<>();

        loginBean = ((LoginBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loginBean"));
        user = loginBean.getUser();
        app = aplicacionFacade.findAppByUsuario(user).get(0);
    }
}
