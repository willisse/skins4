/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skins4.bean;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author masterinftel21
 */
@ManagedBean
@RequestScoped
public class ValidationBean implements Validator {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\."
            + "[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*"
            + "(\\.[A-Za-z]{2,})$";

    private static final String TLFNO_PATTERN = "[6789]\\d{8}";

    private Pattern patternEmail;
    private Pattern patternTlfno;
    private Matcher matcher;

    /**
     * Creates a new instance of ValidationBean
     */
    public ValidationBean() {
        patternEmail = Pattern.compile(EMAIL_PATTERN);
        patternTlfno = Pattern.compile(TLFNO_PATTERN);

    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void validateLogin(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        matcher = patternEmail.matcher(value.toString());

        if ((((String) value).length() == 0)) {
            throw new ValidatorException(new FacesMessage("Login: Introduce un email"));
        } else if ((((String) value).length() > 50) || (((String) value).length() < 2)) {
            throw new ValidatorException(new FacesMessage("Login: Longitud del campo incorrecto"));
        } else if (!matcher.matches()) {
            throw new ValidatorException(new FacesMessage("Login: Direccion Email no valida"));
        }

    }

    public void validatePassword(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        if ((((String) value).length() == 0)) {
            throw new ValidatorException(new FacesMessage("Password: Introduce una contraseña"));
        } else if ((((String) value).length() > 50) || (((String) value).length() < 2)) {
            throw new ValidatorException(new FacesMessage("Password:  Longitud del campo incorrecto"));
        }
    }

    public void validatePasswordMyKong(ComponentSystemEvent event) {

        FacesContext fc = FacesContext.getCurrentInstance();

        UIComponent components = event.getComponent();

        // get password
        UIInput uiInputPassword = (UIInput) components.findComponent("password");
        String password = uiInputPassword.getLocalValue() == null ? "" : uiInputPassword.getLocalValue().toString();
        String passwordId = uiInputPassword.getClientId();

        // get confirm password
        UIInput uiInputConfirmPassword = (UIInput) components.findComponent("password2");
        String confirmPassword = uiInputConfirmPassword.getLocalValue() == null ? "" : uiInputConfirmPassword.getLocalValue().toString();

        // Let required="true" do its job.
        if (password.isEmpty() || confirmPassword.isEmpty()) {
            return;
        }

        if (!password.equals(confirmPassword)) {

            FacesMessage msg = new FacesMessage("Password must match confirm password");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            fc.addMessage(passwordId, msg);
            fc.renderResponse();

        }

    }

    public void validatePassword2(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        String password = value.toString();

        UIInput uiInputConfirmPassword = (UIInput) component.getAttributes().get("password2");
        String password2 = uiInputConfirmPassword.getSubmittedValue().toString();

        System.out.println("2: " + password2 + " 1: " + password);

        // Let required="true" do its job.
        if (password == null || password.isEmpty() || password2 == null || password2.isEmpty()) {
            throw new ValidatorException(new FacesMessage("Password: Introduce una contraseña"));
        } else if (!password.equals(password2)) {
            uiInputConfirmPassword.setValid(false);
            throw new ValidatorException(new FacesMessage("Password: Las contraseñas no coinciden"));
        }
    }

    public void validateNombre(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        if ((((String) value).length() == 0)) {
            throw new ValidatorException(new FacesMessage("Nombre: Introduce un nombre"));
        } else if ((((String) value).length() > 50) || (((String) value).length() < 2)) {
            throw new ValidatorException(new FacesMessage("Nombre:  Longitud del campo incorrecto"));
        }
    }

    public void validateApellido(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        if ((((String) value).length() == 0)) {
            throw new ValidatorException(new FacesMessage("Apellido: Introduce un apellido"));
        } else if ((((String) value).length() > 50) || (((String) value).length() < 2)) {
            throw new ValidatorException(new FacesMessage("Apellido:  Longitud del campo incorrecto"));
        }
    }

    public void validateTelefono(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        matcher = patternTlfno.matcher(value.toString());

        if ((((String) value).length() == 0)) {
            throw new ValidatorException(new FacesMessage("Telefono: Introduce un telefono"));
        } else if ((((String) value).length() > 50) || (((String) value).length() < 2)) {
            throw new ValidatorException(new FacesMessage("Telefono:  Longitud del campo incorrecto"));
        } else if (!matcher.matches()) {
            throw new ValidatorException(new FacesMessage("Login: Direccion Email no valida"));
        }
    }

    public void validateDescripcion(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        if ((((String) value).length() == 0)) {
            throw new ValidatorException(new FacesMessage("Descripción: Introduce una descripción"));
        } else if ((((String) value).length() > 250)) {
            throw new ValidatorException(new FacesMessage("Nombre:  Longitud del campo incorrecto"));
        }
    }

    public void validateNombreGrupo(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if ((((String) value).length() == 0)) {
            throw new ValidatorException(new FacesMessage("Nombre: introduce un nombre para el grupo"));
        } else if ((((String) value).length() > 100)) {
            throw new ValidatorException(new FacesMessage("Nombre:  Longitud del campo incorrecto"));
        }
    }
}
